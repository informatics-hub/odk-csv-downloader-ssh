package net.globalhealthapp.na.odkcsvdownload.odkcsvdwn;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.jcraft.jsch.*;
import android.content.Context;
import android.widget.Button;
import android.widget.Toast;
import android.os.StrictMode;
import android.view.View;
import android.app.ProgressDialog;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        final Button clickButton = (Button) findViewById(R.id.button);
        final int duration = Toast.LENGTH_SHORT;

        if (clickButton != null) {
            clickButton.setOnClickListener(new View.OnClickListener() {


                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Context context = getApplicationContext();
                    CharSequence text2 = "Fetching Data";
                    CharSequence text3 = "File transfer was a success!";
                    CharSequence text4 = "Please wait...";

                    clickButton.setText("Please wait...");
                    clickButton.setEnabled(false);
                    clickButton.clearAnimation();

                   // Toast toast = Toast.makeText(context, text4, duration);
                    //toast.show();
                    if (clickButton.isEnabled())
                    {

                    } else {
                        clickButton.clearAnimation();
                        SSHConnect();
                    }


                    }








                public void SSHConnect() {

                    clickButton.clearAnimation();
                    Context context = getApplicationContext();
                    CharSequence text2 = "Fetching Data";
                    CharSequence text3 = "File transfer was a success!";
                    CharSequence text4 = "Please wait...";

                    try {
                        JSch ssh = new JSch();

                        //Using a private key
                        String privateKey = "/sdcard/sftp/asen_ptracker_server";
                        ssh.addIdentity(privateKey);
                        System.out.println("initialising key auth");

                        // Server address and user info
                        String user = "asen";
                        String host = "ptracker.globalhealthapp.net";
                        int port = 22;

                        Session session = ssh.getSession(user, host, port);

                        java.util.Properties config = new java.util.Properties();
                        config.put("StrictHostKeyChecking", "no");
                        session.setConfig(config);
                        //session.setPassword("Passw0rd");

                        System.out.println("Connecting to server..");
                        session.connect();
                        Channel channel = session.openChannel("sftp");
                        channel.connect();

                        ChannelSftp sftp = (ChannelSftp) channel;

                        //Specify source file and desination file.
                        sftp.get("/home/asen/plist.csv", "/sdcard/odk/forms/PTracker_201705-media/plist.csv");
                        Toast toast2 = Toast.makeText(context, text2, duration);
                        toast2.show();

                        Boolean success = true;

                        if (success) {
                            System.out.println("File transfer was a success!");

                            Toast toast3 = Toast.makeText(context, text3, duration);
                            toast3.show();

                            clickButton.setText("Success!");
                            clickButton.setEnabled(false);
                            clickButton.clearAnimation();

                        }

                        channel.disconnect();
                        session.disconnect();

                    } catch (JSchException e) {
                        System.out.println(e.getMessage().toString());
                        Toast toast4 = Toast.makeText(context, e.getMessage().toString(), duration);
                        toast4.show();
                        clickButton.setText("Retry Download");
                        clickButton.setEnabled(true);
                        clickButton.clearAnimation();

                        e.printStackTrace();
                    } catch (SftpException e) {
                        System.out.println(e.getMessage().toString());
                        e.printStackTrace();
                    }



                }

            });


        }


    }




}
